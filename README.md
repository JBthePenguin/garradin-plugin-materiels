# Plugin Materiels pour Garradin

Cette extension permet de gérer les matériels de l'association: stock, entrées, sorties.

## Installation

- Télécharger le fichier [materiels.tar.gz](https://gitlab.com/JBthePenguin/garradin-plugin-materiels/-/raw/main/materiels.tar.gz) et l'ajouter dans le répertoire garradin/plugins.
- Installer le plugin via le menu "Configuration > Extensions" de Garradin.

## Utilisation

- Ajouter des catégories de matériel (ex: Consommable, Informatique, Bureautique, ...).
- Ajouter des entrées pour du matériel non répertorié.

À partir de là, il est possible pour ces matériels d'ajouter d'autres entrées via matériel répertorié, des sorties via matériel en stock disponible (ou matériel emprunté si le type d'entrée est 'location / prêt').

## Droits d'accès

L'écran est accessible uniquement pour les membres ayant au moins le droit d'écriture en gestion des membres.
