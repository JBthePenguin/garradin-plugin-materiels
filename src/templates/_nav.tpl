<!-- title -->
{include file="admin/_head.tpl" title="%s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
<!-- -->
<!-- nav bar -->
<nav class="tabs">
	<ul>
		<li class="{if $current_nav == 'index'}current{/if}"><a href="{plugin_url}">Inventaire</a></li>
    <li class="{if $current_nav == 'categories'}current{/if}"><a href="{plugin_url file="categories/index.php"}">Catégories</a></li>
		<li class="{if $current_nav == 'entrees'}current{/if}"><a href="{plugin_url file="mouvements/entrees/index.php"}">Entrées</a></li>
		<li class="{if $current_nav == 'sorties'}current{/if}"><a href="{plugin_url file="mouvements/sorties/index.php"}">Sorties</a></li>
	</ul>
</nav>
<!-- -->
