<!-- nav bar -->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="categories"}
<!-- -->
<!-- add form -->
<form method="post" action="{$self_url}">
  <fieldset>
    <legend>Ajouter une catégorie</legend>
    {form_errors}
    <dl>
      {input type="text" name="name" label="Nom" required=true maxlength="100"}
    </dl>
		<p class="submit">
			{csrf_field key=$csrf_key}
			{button type="submit" name="save" label="Ajouter" shape="right" class="main"}
		</p>
  </fieldset>
</form>
<!-- -->
<!-- table with list of categories -->
<table class="list">
	<thead>
		<th><b>Nom</b></th>
		<th></th>
	</thead>
	<tbody>
		{foreach from=$list item="cat"}
			<tr>
				<td>{$cat.name}</td>
				<td class="actions">
          			{linkbutton shape="upload" label="Liste des matériels" href="materiels_par_categorie.php?id=%d"|args:$cat.id}
          			{linkbutton shape="edit" label="Modifier" href="modifier_categorie.php?id=%d"|args:$cat.id}
         			{linkbutton shape="delete" label="Supprimer" href="supprimer_categorie.php?id=%d"|args:$cat.id}
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
