<!-- nav bar -->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="categories"}
<!-- -->
<!-- delete form -->
{include file="%scommon/delete_form.tpl"|args:$plugin_tpl
	legend="Supprimer cette catégorie de matériels ?"
	warning="Êtes-vous sûr de vouloir supprimer la catégorie « %s » ?"|args:$cat_requested.name
	alert="Attention, la catégorie ne doit plus contenir de matériels pour pouvoir être supprimée."
}
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
