<!-- nav bar -->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="categories"}
<!-- -->
<!-- edit form -->
<form method="post" action="{$self_url}" data-focus="1">
  {form_errors}
  <fieldset>
    <legend>Modifier cette catégorie</legend>
    <dl>
      {input type="text" name="name" label="Nom" required=true source=$cat_requested maxlength="100"}
    </dl>
  </fieldset>
  <p class="submit">
		{csrf_field key=$csrf_key}
		{button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    {linkbutton label="Annuler" shape="export" href=$cancel_link}
	</p>
</form>
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
