<!-- nav bar -->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="categories"}
<!-- -->
<!-- title -->
<h2 class="ruler">{$cat_name}</h2>
<!-- -->
<br>
<!-- table with list of equipments owned -->
{if $eqmts_owned}
  <table class="list">
    <thead>
      <tr>
        <th colspan="4"><h3 class="ruler">Matériel dont l'association est propriétaire</h3></th>
      </tr>
      <tr>
        <th><b>Désignation</b></th>
        <th class="num"><b>Stock</b></th>
        <th class="num"><b>Sortie</b></th>
        <th class="num"><b>Disponible</b></th>
      </tr>
    </thead>
    <tbody>
      {foreach from=$eqmts_owned item="eqmt"}
        <tr>
          <td>{$eqmt.designation}</td>
          <td class="num">{$eqmt.owned}</td>
          <td class="num">{$eqmt.owned_out}</td>
          <td class="num">{$eqmt.owned - $eqmt.owned_out}</td>
        </tr>
      {/foreach}
    </tbody>
  </table>
{/if}
<!-- -->
<!-- table with list of equipments no owned -->
{if $eqmts_no_owned}
  <table class="list">
    <thead>
      <tr>
        <th colspan="2"><h3 class="ruler">Matériel dont l'association n'est pas propriétaire (emprunté)</h3></th>
      </tr>
      <tr>
        <th><b>Désignation</b></th>
        <th class="num"><b>Nombre</b></th>
			</tr>
			</thead>
			<tbody>
				{foreach from=$eqmts_no_owned item="eqmt"}
					<tr>
            <td>{$eqmt.designation}</td>
            <td class="num">{$eqmt.no_owned}</td>
          </tr>
        {/foreach}
      </tbody>
    </table>
{/if}
<!-- -->
<!-- table with list of equipments just listed -->
{if $eqmts_just_listed}
  <table class="list">
    <thead>
      <tr>
        <th><h3  class="ruler">Matériel dont l'association n'est plus en possession</h3></th>
      </tr>
      <tr>
        <th><b>Désignation</b></th>
      </tr>
    </thead>
    <tbody>
      {foreach from=$eqmts_just_listed item="eqmt"}
        <tr>
          <td>{$eqmt.designation}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
{/if}
<!-- -->
{linkbutton label="Retour" shape="export" href=$return_link}
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
