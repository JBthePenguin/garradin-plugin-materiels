<!-- nav bar -->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="index"}
<!-- -->
<h2 class="ruler">Matériel dont l'association est propriétaire</h2>

<br>
<!-- tables for equipments owned -->
{foreach from=$eqmts_owned_by_cat key='cat' item="eqmts"}
	{if $eqmts}
		<table class="list">
			<thead>
				<tr>
					<th colspan="5"><h3 class="ruler">{$cat}</h3></th>
				</tr>
				<tr>
					<th><b>Désignation</b></th>
					<th class="num"><b>Stock</b></th>
					<th class="num"><b>Sortie</b></th>
					<th class="num"><b>Disponible</b></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$eqmts item="eqmt"}
					<tr>
						<td>{$eqmt.designation}</td>
						<td class="num">{$eqmt.owned}</td>
						<td class="num">{$eqmt.owned_out}</td>
						<td class="num">{$eqmt.owned - $eqmt.owned_out}</td>
						<td class="actions">
							{linkbutton shape="edit" label="Historique des entrées / sorties" href="historique.php?id=%d"|args:$eqmt.id}
          					{linkbutton shape="edit" label="Modifier" href="modifier_materiel.php?id=%d"|args:$eqmt.id}
						</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	{/if}
{/foreach}
<!-- -->
<br>

<hr>

<br>

<h2 class="ruler">Matériel dont l'association n'est pas propriétaire (emprunté)</h2>

<br>
<!-- tables for equipments no owned -->
{foreach from=$eqmts_no_owned_by_cat key='cat' item="eqmts"}
	{if $eqmts}
		<table class="list">
			<thead>
				<tr>
					<th colspan="3"><h3 class="ruler">{$cat}</h3></th>
				</tr>
				<tr>
					<th><b>Désignation</b></th>
					<th class="num"><b>Nombre</b></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$eqmts item="eqmt"}
					<tr>
						<td>{$eqmt.designation}</td>
						<td class="num">{$eqmt.no_owned}</td>
						<td class="actions">
							{linkbutton shape="edit" label="Historique des entrées / sorties" href="historique.php?id=%d"|args:$eqmt.id}
          					{linkbutton shape="edit" label="Modifier" href="modifier_materiel.php?id=%d"|args:$eqmt.id}
						</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	{/if}
{/foreach}
<!-- -->
<br>

<hr>

<br>

<h2 class="ruler">Matériel dont l'association n'est plus en possession</h2>

<br>
<!-- tables for equipments just listed -->
{foreach from=$eqmts_just_listed_by_cat key='cat' item="eqmts"}
	{if $eqmts}
		<table class="list">
			<thead>
				<tr>
					<th colspan="2"><h3 class="ruler">{$cat}</h3></th>
				</tr>
				<tr>
					<th><b>Désignation</b></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$eqmts item="eqmt"}
					<tr>
						<td>{$eqmt.designation}</td>
						<td class="actions">
							{linkbutton shape="edit" label="Historique des entrées / sorties" href="historique.php?id=%d"|args:$eqmt.id}
          					{linkbutton shape="edit" label="Modifier" href="modifier_materiel.php?id=%d"|args:$eqmt.id}
						</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	{/if}
{/foreach}
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
