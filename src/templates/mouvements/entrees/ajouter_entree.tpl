<!-- nav bar -->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="entrees"}
<!-- -->
<!-- add entry form -->
{include file="%smouvements/formulaire_mouvement.tpl"|args:$plugin_tpl legend="entrée d'un matériel %s"|args:$legend_part tpl_materiel_name=$tpl_materiel_name kinds=$kinds selected_kind=$selected_kind default_date=$default_date label_date="Date d'entrée" tpl_materiel_path="entrees" comment_placeholder=$comment_placeholder csrf_key=$csrf_key cancel_link=$cancel_link}
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
