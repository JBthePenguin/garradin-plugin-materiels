<!-- nav bar-->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="entrees"}
<!-- -->
<!-- add entry links -->
<fieldset>
  <legend>Ajouter une entrée pour du </legend>
    {linkbutton shape="plus" label="Matériel répertorié" href="repertorie.php"}
    {linkbutton shape="plus" label="Matériel non répertorié" href="non_repertorie.php"}
    {linkbutton shape="plus" label="Matériel en retour de location / prêt" href="retour.php"}
</fieldset>
<!-- -->
<!-- entries table -->
{include file="%smouvements/tableau_mouvements.tpl"|args:$plugin_tpl mvts=$mvts del_href="supprimer_entree.php?id=%d"}
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
