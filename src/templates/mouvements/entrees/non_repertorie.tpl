<!-- materiel not listed part for add movement form -->
<fieldset>
  <legend><h3>Matériel</h3></legend>
  <dl>
    <dt><label for="f_cat">Catégorie</label> <b>(obligatoire)</b></dt>
    <dd>
      <select name="category_id" id="f_cat">
        {foreach from=$cats item="cat"}
          <option value="{$cat.id}"{if $selected_cat == $cat.id} selected="selected"{/if}>{$cat.name}</option>
        {/foreach}
      </select>
    </dd>
    {input type="text" name="designation" label="Désignation" required=true maxlength="255"}
  </dl>
</fieldset>
<!-- -->
