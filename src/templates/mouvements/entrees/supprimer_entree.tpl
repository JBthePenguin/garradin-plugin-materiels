<!-- nav bar -->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="entrees"}
<!-- -->
<!-- delete form -->
{include file="%scommon/delete_form.tpl"|args:$plugin_tpl
	legend="Supprimer cette entrée de matériels ?"
	warning="Êtes-vous sûr de vouloir supprimer l'entrée de « %s » ?"|args:$entry_string
	alert="Attention, la suppression de cette entrée supprimera le matériel « %s » du répertoire s'il n'a pas d'autre entrée."|args:$eqmt_name
}
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
