<!-- materiel listed part for add movement form -->
<fieldset>
  <legend><h3>Matériel</h3></legend>
  <dl>
    <dt><label for="f_eqmt"></label> <b>(obligatoire)</b></dt>
    <dd>
      <select name="equipment_id" id="f_eqmt">
        {foreach from=$eqmts_by_cat key='cat' item="eqmts"}
          {if $eqmts}
            <optgroup label="-- {$cat} --">
              {foreach from=$eqmts item="eqmt"}
                <option value="{$eqmt.id}">{$eqmt.designation}</option>
              {/foreach}
            </optgroup>
          {/if}
        {/foreach}
      </select>
    </dd>
  </dl>
</fieldset>
<!-- -->
