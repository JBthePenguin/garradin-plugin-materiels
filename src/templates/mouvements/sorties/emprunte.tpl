<!-- materiel borrowed for output part for add movement form -->
<fieldset>
  <legend><h3>Matériel</h3></legend>
  <dl>
    <dt><label for="f_eqmt"></label> <b>(obligatoire)</b></dt>
    <dd>
      <select name="equipment_id" id="f_eqmt">
        {foreach from=$eqmts_by_cat key='cat' item="eqmts"}
          <optgroup label="-- {$cat} --">
            {foreach from=$eqmts item="eqmt"}
              <option value="{$eqmt.id}"{if ($selected_eqmt !== "") && ($selected_eqmt == $eqmt.id)} selected="selected"{/if}>{$eqmt.designation} - emprunt: {$eqmt.borrowed}</option>
            {/foreach}
          </optgroup>
        {/foreach}
      </select>
    </dd>
  </dl>
</fieldset>
<!-- -->
