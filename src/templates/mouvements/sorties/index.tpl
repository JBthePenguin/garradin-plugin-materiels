<!-- nav bar-->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="sorties"}
<!-- -->
<!-- add output links -->
<fieldset>
  <legend>Ajouter une sortie pour du </legend>
    {linkbutton shape="plus" label="Matériel en stock disponible" href="stock_disponible.php"}
    {linkbutton shape="plus" label="Matériel emprunté" href="emprunte.php"}
</fieldset>
<!-- -->
<!-- entries table -->
{include file="%smouvements/tableau_mouvements.tpl"|args:$plugin_tpl mvts=$mvts del_href="supprimer_sortie.php?id=%d"}
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
