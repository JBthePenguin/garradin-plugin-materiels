<!-- nav bar -->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="sorties"}
<!-- -->
<!-- delete form -->
{include file="%scommon/delete_form.tpl"|args:$plugin_tpl
	legend="Supprimer cette sortie de matériels ?"
	warning="Êtes-vous sûr de vouloir supprimer la sortie de « %s » ?"|args:$output_string
  alert=""
}
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
