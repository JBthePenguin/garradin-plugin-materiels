<!-- table of movements -->
{if $mvts}
  <table class="list">
    <thead>
      <th><b>Date</b></th>
      <th><b>Type</b></th>
      <th class="num"><b>Nombre</b></th>
      <th><b>Matériel</b></th>
      <th><b>Remarques</b></th>
      <th></th>
    </thead>
    <tbody>
      {foreach from=$mvts item="mvt"}
        <tr>
          <td>{$mvt.mvt_date|date_format:'%d/%m/%y'}</td>
          <td>{$mvt.kind}</td>
          <td class="num">{$mvt.equipment_number}</td>
          <td>{$mvt.equipment}</td>
          <td>{$mvt.additional_comment}</td>
          <td class="actions">
            {linkbutton shape="delete" label="Supprimer" href=$del_href|args:$mvt.id}
          </td>
        </tr>
      {/foreach}
    </tbody>
  </table>
{/if}
<!-- -->
