<!-- add movement form -->
<form method="post" action="{$self_url}">
	<fieldset>
    <!-- legend -->
		<legend>Ajouter une {$legend}</legend>
    <!-- -->
    <!-- errors -->
		{form_errors}
    <!-- -->
    <dl>
			{if $kinds}
        <!-- kind -->
      	<dt><label for="f_kind">Type</label> <b>(obligatoire)</b></dt>
				<dd>
      		<select name="kind" id="f_kind">
        		{foreach from=$kinds item="kind"}
          		<option value="{$kind}"{if $selected_kind == $kind} selected="selected"{/if}>{$kind}</option>
          	{/foreach}
        	</select>
      	</dd>
        <!-- -->
			{/if}
      <!-- date -->
      {input type="date" name="mvt_date" default=$default_date label=$label_date required=true}
      <!-- -->
      <!-- number -->
			{input type="number" name="equipment_number" label="Nombre" required=true step="1" min="1" default="1"}
      <!-- -->
    </dl>
    <!-- materiel -->
		{include file="%smouvements/{$tpl_materiel_path}/{$tpl_materiel_name}.tpl"|args:$plugin_tpl}
    <!-- -->
    <!-- comment -->
		<dl>
			{input type="textarea" name="additional_comment" label="Remarques" placeholder=$comment_placeholder default="" maxlength="255" rows=4 cols=30}
		</dl>
    <!-- -->
	</fieldset>
  <!-- submit and cancel buttons-->
	<p class="submit">
		{csrf_field key=$csrf_key}
		{button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    {linkbutton label="Annuler" shape="export" href=$cancel_link}
	</p>
  <!-- -->
</form>
<!-- -->
