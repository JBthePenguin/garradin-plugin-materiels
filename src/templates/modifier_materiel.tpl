<!-- nav bar -->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="index"}
<!-- -->
<!-- edit form -->
<form method="post" action="{$self_url}" data-focus="1">
  {form_errors}
  <fieldset>
    <legend>Modifier ce matériel</legend>
    <dl>
      <dt><label for="f_cat">Catégorie</label> <b>(obligatoire)</b></dt>
      <dd>
        <select name="category_id" id="f_cat">
          {foreach from=$cats item="cat"}
            <option value="{$cat.id}"{if $selected_cat == $cat.id} selected="selected"{/if}>{$cat.name}</option>
          {/foreach}
        </select>
      </dd>
      {input type="text" name="designation" label="Désignation" required=true source=$eqmt_requested maxlength="255"}
    </dl>
  </fieldset>
  <p class="submit">
		{csrf_field key=$csrf_key}
		{button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    {linkbutton label="Annuler" shape="export" href=$cancel_link}
	</p>
</form>
<!-- -->
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
