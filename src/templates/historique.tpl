<!-- nav bar-->
{include file="%s_nav.tpl"|args:$plugin_tpl current_nav="index"}
<!-- -->
<!-- -->
<h2  class="ruler">Historique des entrées / sorties</h2>

<br>

<!-- table of movements -->
{if $mvts}
  <table class="list">
    <thead>
      <tr>
        <th colspan="5">
          <h3  class="ruler">{$eqmt_requested.designation} | {$eqmt_requested.category}</h3>
        </th>
      </tr>
      <tr>
        <th><b>Date</b></th>
        <th><b>Sens</b></th>
        <th><b>Type</b></th>
        <th class="num"><b>Nombre</b></th>
        <th><b>Remarques</b></th>
      </tr>
    </thead>
    <tbody>
      {foreach from=$mvts item="mvt"}
        <tr>
          <td>{$mvt.mvt_date|date_format:'%d/%m/%y'}</td>
          {if $mvt.side}
            <td>Sortie</td>
          {else}
            <td>Entrée</td>
          {/if}
          <td>{$mvt.kind}</td>
          <td class="num">{$mvt.equipment_number}</td>
          <td>{$mvt.additional_comment}</td>
        </tr>
      {/foreach}
    </tbody>
  </table>
{/if}
<!-- -->
{linkbutton label="Retour" shape="export" href=$return_link}
<!-- footer -->
{include file="admin/_foot.tpl"}
<!-- -->
