<!-- delete form -->
<form method="post" action="{$self_url}" data-focus="1">
  {form_errors}
  <fieldset>
	   <legend>{$legend}</legend>
	    <h3 class="warning">{$warning}</h3>
      {if $alert != ""}
		    <p class="block alert">{$alert}</p>
      {/if}
	</fieldset>
	<p class="submit">
		{csrf_field key=$csrf_key}
		{button type="submit" name="delete" label="Supprimer" shape="delete" class="main"}
    {linkbutton label="Annuler" shape="export" href=$cancel_link}
	</p>
</form>
<!-- -->
