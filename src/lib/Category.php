<?php

namespace Garradin\Plugin\Materiels;

use Garradin\DB;

class Category
{
  public function add($data = [])
  // add new category
	{
		DB::getInstance()->insert('plugin_materiels_category', $data);
	}

  public function edit($id, $data = [])
  // edit a specific category
	{
    $db = DB::getInstance();
    $db->update('plugin_materiels_category', $data, $db->where('id', $id));
	}

  public function delete($id)
  // delete a specific category
	{
		DB::getInstance()->delete('plugin_materiels_category', 'id = ?', $id);
	}

  public function get($id)
  // get and return a specific category
  {
    return DB::getInstance()->first('SELECT * FROM plugin_materiels_category WHERE id = ?;', $id);
  }

  public function listAll()
  // return a list of all categories ordered by name
	{
		return DB::getInstance()->get('SELECT * FROM plugin_materiels_category ORDER BY name;');
	}

  public function listAllEquipments($id)
  // return a list of all equipments for a specific category ordered by equipments's designation
  {
    return DB::getInstance()->get(
        'SELECT * FROM plugin_materiels_equipment WHERE category_id = ? ORDER BY designation;', $id);
  }
}
