<?php

namespace Garradin\Plugin\Materiels;

use Garradin\DB;
use Garradin\Plugin\Materiels\Category;

class Equipment
{
  public function add($data = [])
  // add new equipment and return his id
	{
    $db = DB::getInstance();
		$db->insert('plugin_materiels_equipment', $data);
    return $db->lastInsertRowId();
	}

  public function edit($id, $data = [])
  // edit a specific equipment
	{
    $db = DB::getInstance();
    $db->update('plugin_materiels_equipment', $data, $db->where('id', $id));
	}

  public function get($id)
  // get and return a specific category
  {
    return DB::getInstance()->first('SELECT * FROM plugin_materiels_equipment WHERE id = ?;', $id);
  }

  public function delete($id)
  // delete a specific equipment
	{
		DB::getInstance()->delete('plugin_materiels_equipment', 'id = ?', $id);
	}

  public function listAllByCategory()
  // return list of all equipments ordered by category name
  {
    // get list of all category
    $category = new Category;
    $cats = $category->listAll();
    // construct list of all equipments ordered by category name and return it
    $eqmts_by_cat = array();
    foreach ($cats as $cat) {
    	$eqmts_by_cat[$cat->name] = $category->listAllEquipments($cat->id);
    }
    return $eqmts_by_cat;
  }

  public function CalculateOwned($id)
  // return number of equipments owned
  {
    $entries = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '0' AND kind IN (
        'Achat', 'Don', 'Récupération') AND equipment_id = ?;", $id);
    $outputs = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '1' AND kind IN (
        'Vente', 'Don', 'Besoin', 'Autre (perte, vol, ...)') AND equipment_id = ?;", $id);
    return $entries - $outputs;
  }

  public function CalculateOwnedOut($id)
  // return number of equipments owned out
  {
    $entries = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '0' AND kind = 'Retour de location / prêt' AND equipment_id = ?;", $id);
    $outputs = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '1' AND kind = 'Location / Prêt' AND equipment_id = ?;", $id);
    return $outputs - $entries;
  }

  public function CalculateNoOwned($id)
  // return number of equipments no owned
  {
    $entries = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '0' AND kind = 'Location / Prêt' AND equipment_id = ?;", $id);
    $outputs = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '1' AND kind = 'Retour de location / prêt' AND equipment_id = ?;", $id);
    return $entries - $outputs;
  }

  public function AllListsAll($eqmts)
  // construct and return 3 lists with a specific list of equipments:
  // equipments owned, equipments no owned and equipments just listed
  {
    $eqmts_owned = array();
    $eqmts_no_owned = array();
    $eqmts_just_listed = array();
    foreach ($eqmts as $eqmt) {
      $owned = $this->CalculateOwned($eqmt->id);
      if ($owned) {
        // add equipment to list of equiments owned with his number and number of out
        $eqmt->owned = $owned;
        $owned_out = $this->CalculateOwnedOut($eqmt->id);
        if ($owned_out) {
          $eqmt->owned_out = $owned_out;
        } else {
          $eqmt->owned_out = 0;
        }
        array_push($eqmts_owned, $eqmt);
      }
      $no_owned = $this->CalculateNoOwned($eqmt->id);
      if ($no_owned) {
        // add equipment to list of equiments no owned with his number
        $eqmt->no_owned = $no_owned;
        array_push($eqmts_no_owned, $eqmt);
      }
      if ($owned + $no_owned == 0) {
        // add equipment to list of equiments just listed
        array_push($eqmts_just_listed, $eqmt);
      }
    }
    return array(
      $eqmts_owned, $eqmts_no_owned, $eqmts_just_listed
    );
  }

  public function AllListsAllByCategory()
  // construct and return 3 lists with all equipments:
  // equipments owned, equipments no owned and equipments just listed
  // ordered by category
  {
    // get list of all equipments ordered by category
    $eqmts_by_cat = $this->listAllByCategory();
    // construct the 3 lists
    $eqmts_owned_by_cat = array();
    $eqmts_no_owned_by_cat = array();
    $eqmts_just_listed_by_cat = array();
    foreach ($eqmts_by_cat as $cat => $eqmts) {
      // for each category construct the 3 lists with all of his equipments
      list($eqmts_owned, $eqmts_no_owned, $eqmts_just_listed) = $this->AllListsAll($eqmts);
      $eqmts_owned_by_cat[$cat] = $eqmts_owned;
      $eqmts_no_owned_by_cat[$cat] = $eqmts_no_owned;
      $eqmts_just_listed_by_cat[$cat] = $eqmts_just_listed;
    }
    return array(
      $eqmts_owned_by_cat, $eqmts_no_owned_by_cat,
      $eqmts_just_listed_by_cat
    );
  }

  public function CalculateAvailable($id)
  // return the number of available for a specific equipment
  {
    $entries = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '0' AND kind IN (
        'Achat', 'Don', 'Récupération',
        'Retour de location / prêt') AND equipment_id = ?;", $id);
    $outputs = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '1' AND kind IN (
        'Vente', 'Don', 'Besoin', 'Autre (perte, vol, ...)',
        'Location / Prêt') AND equipment_id = ?;", $id);
    return $entries - $outputs;
  }

  public function ListAllAvailableByCategory()
  // return list of all available equipments ordered by category
  {
    // get list of all equipments ordered by category
    $eqmts_by_cat = $this->listAllByCategory();
    // construct list of all available equipments ordered by category
    $eqmts_available_by_cat = array();
    foreach ($eqmts_by_cat as $cat => $eqmts) {
      // for each category construct list of his available equipments
      $eqmts_available = array();
      foreach ($eqmts as $eqmt) {
        $available = $this->CalculateAvailable($eqmt->id);
        if ($available) {
          $eqmt->available = $available;
          array_push($eqmts_available, $eqmt);
        }
      }
      if ($eqmts_available) {
        $eqmts_available_by_cat[$cat] = $eqmts_available;
      }
    }
    return $eqmts_available_by_cat;
  }

  public function CalculateAvailableByDate($id, $date)
  // return the number of available equipments (owned) at a specific date
  {
    $entries = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '0' AND kind IN (
        'Achat', 'Don', 'Récupération',
        'Retour de location / prêt') AND equipment_id = ? AND mvt_date <= ?;", $id, $date);
    $outputs = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '1' AND kind IN (
        'Vente', 'Don', 'Besoin', 'Autre (perte, vol, ...)',
        'Location / Prêt') AND equipment_id = ? AND mvt_date <= ?;", $id, $date);
    return $entries - $outputs;
  }

  public function listAllReleasedRentByCategory()
  // return list of equipments for possible return (loan or rent) ordered by category
  {
    // get the list of all equipments ordered by category
    $eqmts_by_cat = $this->listAllByCategory();
    // construct list of equipments for possible return for all categories
    $eqmts_released_by_cat = array();
    foreach ($eqmts_by_cat as $cat => $eqmts) {
      // construct list of equipments for possible return for one category
      $eqmts_released = array();
      foreach ($eqmts as $eqmt) {
        // for each equipment calculte number of possible return
        $released = $this->CalculateOwnedOut($eqmt->id);
        if ($released) {
          // add to the list if at least one is possible for return
          $eqmt->released = $released;
          array_push($eqmts_released, $eqmt);
        }
      }
      if ($eqmts_released) {
        $eqmts_released_by_cat[$cat] = $eqmts_released;
      }
    }
    return $eqmts_released_by_cat;
  }

  public function CalculateOutOfStockByDate($id, $date)
  // return number of a specific equipment is out at a specific date
  {
    $entries = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '0' AND kind = 'Retour de location / prêt' AND equipment_id = ? AND mvt_date <= ?;", $id, $date);
    $outputs = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '1' AND kind = 'Location / Prêt' AND equipment_id = ? AND mvt_date <= ?;", $id, $date);
    return $outputs - $entries;
  }

  public function ListAllBorrowedByCategory()
  // return list of borrowed equipments ordered by category
  {
    // get the list of all equipments ordered by category
    $eqmts_by_cat = $this->listAllByCategory();
    // construct list of equipments borrowed for all categories
    $eqmts_borrowed_by_cat = array();
    foreach ($eqmts_by_cat as $cat => $eqmts) {
      // construct list of equipments borrowed for one category
      $eqmts_borrowed = array();
      foreach ($eqmts as $eqmt) {
        // for each equipment calculte number of borrowed
        $borrowed = $this->CalculateNoOwned($eqmt->id);
        if ($borrowed) {
          // add to the list if at least one is borrowed
          $eqmt->borrowed = $borrowed;
          array_push($eqmts_borrowed, $eqmt);
        }
      }
      if ($eqmts_borrowed) {
        $eqmts_borrowed_by_cat[$cat] = $eqmts_borrowed;
      }
    }
    return $eqmts_borrowed_by_cat;
  }

  public function CalculateNoOwnedByDate($id, $date)
  // return the number of equipments no owned at a specific date
  {
    $entries = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '0' AND kind = 'Location / Prêt' AND equipment_id = ? AND mvt_date <= ?;", $id, $date);
    $outputs = DB::getInstance()->firstColumn(
      "SELECT sum(equipment_number) FROM plugin_materiels_movement WHERE side = '1' AND kind = 'Retour de location / prêt' AND equipment_id = ? AND mvt_date <= ?;", $id, $date);
    return $entries - $outputs;
  }
}
