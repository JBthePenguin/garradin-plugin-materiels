<?php

namespace Garradin\Plugin\Materiels;

use Garradin\DB;
use Garradin\Plugin\Materiels\Equipment;

class Movement
{
  public function add($data = [])
  // add new movement
	{
    DB::getInstance()->insert('plugin_materiels_movement', $data);
	}

  public function get($id)
  // get and return a specific movement
  {
    return DB::getInstance()->first('SELECT * FROM plugin_materiels_movement WHERE id = ?;', $id);
  }

  public function delete($id)
  // delete specific movement
	{
		DB::getInstance()->delete('plugin_materiels_movement', 'id = ?', $id);
	}

  public function listEntryKinds()
  // return list of entry's kinds
  {
    return array(
      'Achat',
      'Don',
      'Récupération',
      'Location / Prêt',
    );
  }

  public function listOutputKinds()
  // return list of output's kinds
  {
    return array(
      'Vente',
      'Don',
      'Besoin',
      'Autre (perte, vol, ...)',
      'Location / Prêt',
    );
  }

  public function listAllOneSide($side)
  // return all entries if side is 0 or all outputs if side is 1 ordered by date
	{
		$mvts = DB::getInstance()->get("SELECT * FROM plugin_materiels_movement WHERE side = ? ORDER BY mvt_date DESC;", $side);
    // for each movements get the equipment's designation
    $eqmt = new Equipment;
    foreach ($mvts as $key => $value) {
    	$mvt_eqmt = $eqmt->get($value->equipment_id);
    	$mvts[$key]->equipment = $mvt_eqmt->designation;
    }
    return $mvts;
	}

  public function PossibilityRentEqmtEntry($id, $eqmt_number, $date)
  // check if it's possible to add a return entry
  // at specific date for a specific number
  // return true if it's ok else return false
	{
    // get all entry dates after the specific date and make a list with its
    $after_entry_dates = DB::getInstance()->get(
      "SELECT mvt_date FROM plugin_materiels_movement WHERE side = '0' AND kind = 'Retour de location / prêt' AND equipment_id = ? AND mvt_date > ?;", $id, $date);
    $entry_dates = array($date);
    foreach ($after_entry_dates as $row)
    {
      array_push($entry_dates, $row->mvt_date);
    }
    $eqmt = new Equipment;
    foreach ($entry_dates as $entry_date) {
      // for each date check if it's possible to add this number of return entries
      $out_of_stock_eqmt = $eqmt->CalculateOutOfStockByDate($id, $entry_date);
      if ($out_of_stock_eqmt - $eqmt_number < 0)
      {
        // not possible
        return false;
      }
    }
    // possible for all dates
    return true;
	}

  public function PossibilityOwnedEqmtOutput($id, $eqmt_number, $date)
  // check if it's possible to add an output for a specific equipment owned
  // at specific date for a specific number
  // return true if it's ok else return false
	{
    // get all output dates after the specific date and make a list with its
    $after_output_dates = DB::getInstance()->get(
      "SELECT mvt_date FROM plugin_materiels_movement WHERE side = '1' AND kind IN (
        'Vente', 'Don', 'Besoin', 'Autre (perte, vol, ...)',
        'Location / Prêt') AND equipment_id = ? AND mvt_date > ?;", $id, $date);
    $output_dates = array($date);
    foreach ($after_output_dates as $row)
    {
      array_push($output_dates, $row->mvt_date);
    }
    $eqmt = new Equipment;
    foreach ($output_dates as $output_date) {
      // for each date check if it's possible to add this number of output
      $available_eqmt = $eqmt->CalculateAvailableByDate($id, $output_date);
      if ($available_eqmt - $eqmt_number < 0)
      {
        // not possible
        return false;
      }
    }
    // possible for all dates
    return true;
	}

  public function PossibilityNoOwnedEqmtOutput($id, $eqmt_number, $date)
  // check if it's possible to add an output for a specific equipment no owned
  // at specific date for a specific number
  // return true if it's ok else return false
	{
    // get all output dates after the specific date and make a list with its
    $after_output_dates = DB::getInstance()->get(
      "SELECT mvt_date FROM plugin_materiels_movement WHERE side = '1' AND kind = 'Retour de location / prêt' AND equipment_id = ? AND mvt_date > ?;", $id, $date);
    $output_dates = array($date);
    foreach ($after_output_dates as $row)
    {
      array_push($output_dates, $row->mvt_date);
    }
    $eqmt = new Equipment;
    foreach ($output_dates as $output_date) {
      // for each date check if it's possible to add this number of output
      $borrowed_eqmt = $eqmt->CalculateNoOwnedByDate($id, $output_date);
      if ($borrowed_eqmt - $eqmt_number < 0)
      {
        // not possible
        return false;
      }
    }
    // possible for all dates
    return true;
	}

  public function PossibilityDeleteEntry($entry)
  // return true if it's possible to delete this entry, else return false
	{
    if ($entry->kind == 'Location / Prêt')
    {
      return $this->PossibilityNoOwnedEqmtOutput(
        $entry->equipment_id, $entry->equipment_number, $entry->mvt_date);
    } else
    {
      return $this->PossibilityOwnedEqmtOutput(
        $entry->equipment_id, $entry->equipment_number, $entry->mvt_date);
    }
	}

  public function PossibilityDeleteOutput($output)
  // return true if it's possible to delete this entry, else return false
	{
    if ($output->kind == 'Location / Prêt')
    {
      return $this->PossibilityRentEqmtEntry(
        $output->equipment_id, $output->equipment_number, $output->mvt_date);
    }
    return true;
	}

  public function AllEqmtMovements($eqmt_id)
  // return list of all movements for a specific equipment
  // ordered by date and side
  // if same date order by rent output and after put entry for return
	{
    return DB::getInstance()->get(
      "SELECT * FROM plugin_materiels_movement WHERE equipment_id = ? ORDER BY mvt_date DESC, CASE WHEN (side = '1' AND kind = 'Location / Prêt') THEN 1 WHEN (side = '0' AND kind != 'Retour de location / prêt') THEN 2 ELSE 0 END, side DESC;", $eqmt_id);
	}
}
