CREATE TABLE IF NOT EXISTS plugin_materiels_category (
	id                   integer NOT NULL  PRIMARY KEY autoincrement,
	name                 varchar(100) NOT NULL,
	CONSTRAINT u_category_name UNIQUE ( name )
);

CREATE TABLE IF NOT EXISTS plugin_materiels_equipment (
	id                   integer NOT NULL  PRIMARY KEY autoincrement,
	category_id          integer NOT NULL,
	designation          varchar(255) NOT NULL,
	CONSTRAINT u_equipment_designation UNIQUE ( designation ),
	FOREIGN KEY ( category_id ) REFERENCES plugin_materiels_category( id ) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS plugin_materiels_movement (
	id                   integer NOT NULL  PRIMARY KEY autoincrement,
	side                 boolean NOT NULL CHECK(side IN(0,1)),
	kind                 varchar(20) NOT NULL,
	equipment_number     integer NOT NULL,
	equipment_id         integer NOT NULL,
	mvt_date             date NOT NULL,
	additional_comment   varchar(255),
	FOREIGN KEY ( equipment_id ) REFERENCES plugin_materiels_equipment( id ) ON DELETE RESTRICT ON UPDATE CASCADE
);
