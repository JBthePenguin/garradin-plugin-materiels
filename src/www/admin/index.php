<?php

// lists of all equiments owned, no owned and just listed
// ordered by category for each one.

namespace Garradin;

if ($plugin->needUpgrade())
{
	$plugin->upgrade();
}

require_once __DIR__ . '/_inc.php';

use Garradin\Plugin\Materiels\Equipment;

$eqmt = new Equipment;

// get all lists
list(
	$eqmts_owned_by_cat, $eqmts_no_owned_by_cat,
	$eqmts_just_listed_by_cat) = $eqmt->AllListsAllByCategory();

// send lists to template
$tpl->assign(compact(
	'eqmts_owned_by_cat', 'eqmts_no_owned_by_cat',
	'eqmts_just_listed_by_cat'
));

$tpl->display(PLUGIN_ROOT . '/templates/index.tpl');
