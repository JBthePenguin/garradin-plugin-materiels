<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

use Garradin\Utils;

// get the category selected

$cat_requested = $cat->get((int) qg('id'));

if (!$cat_requested) {
	throw new UserException("Cette catégorie n'existe pas.");
}

// check if delete form is submitted

$csrf_key = 'delete_category_' . $cat_requested->id;

if (f('delete') && $form->check($csrf_key) && !$form->hasErrors())
{
	try
	{
		// try to delete category selected and if error catched add it in form
		$cat->delete($cat_requested->id);
		Utils::redirect(PLUGIN_URL . 'categories/index.php');
	}
	catch (\RuntimeException $e)
	{
		if (strstr($e->getMessage(), 'FOREIGN KEY constraint failed'))
		{
			$form->addError('Cette catégorie contient des matériels et ne peut donc pas être supprimée.');
		} else
		{
			$form->addError($e->getMessage());
		}
	}
}

$cancel_link = PLUGIN_URL . 'categories/index.php';

// send to template the category requested

$tpl->assign(compact('cat_requested', 'csrf_key', 'cancel_link'));

$tpl->display(PLUGIN_ROOT . '/templates/categories/supprimer_categorie.tpl');
