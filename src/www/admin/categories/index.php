<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

use Garradin\Utils;

// check if add form is submitted

$csrf_key = 'cat_create';

if (f('save') && $form->check($csrf_key) && !$form->hasErrors())
{
	// try to add new category and if error catched add it in form
	try
	{
		$cat->add([
			'name' => ucfirst(f('name'))
		]);
		Utils::redirect(PLUGIN_URL . 'categories/index.php');
	}
	catch (\RuntimeException $e)
	{
		if (strstr($e->getMessage(), 'UNIQUE constraint failed'))
		{
			$form->addError('Cette catégorie existe déjà.');
		} else
		{
			$form->addError($e->getMessage());
		}
	}
}

// get the list of all categories and send it to template

$list = $cat->listAll();

$tpl->assign(compact('csrf_key', 'list'));

$tpl->display(PLUGIN_ROOT . '/templates/categories/index.tpl');
