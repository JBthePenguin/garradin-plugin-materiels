<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

use Garradin\Plugin\Materiels\Equipment;

// get the category requested and his name

$cat_requested = $cat->get((int) qg('id'));

if (!$cat_requested) {
	throw new UserException("Cette catégorie n'existe pas.");
}

$cat_name = $cat_requested->name;

// get the list of all equipments for the requested category

$eqmts = $cat->listAllEquipments($cat_requested->id);

// get all lists of equipments founded

$eqmt = new Equipment;

list($eqmts_owned, $eqmts_no_owned, $eqmts_just_listed) = $eqmt->AllListsAll($eqmts);

$return_link = PLUGIN_URL . 'categories/index.php';

// send to template the category's name and all lists of its equipment

$tpl->assign(compact(
	'cat_name', 'eqmts_owned', 'eqmts_no_owned', 'eqmts_just_listed', 'return_link'));

$tpl->display(PLUGIN_ROOT . '/templates/categories/materiels_par_categorie.tpl');
