<?php

// edit a specific category

namespace Garradin;

require_once __DIR__ . '/_inc.php';

use Garradin\Utils;

// get the category requested
$cat_requested = $cat->get((int) qg('id'));

if (!$cat_requested) {
	throw new UserException("Cette catégorie n'existe pas.");
}

// check if edit form is submitted
$csrf_key = 'edit_category_' . $cat_requested->id;

if (f('save') && $form->check($csrf_key) && !$form->hasErrors())
{
	// try to edit category selected and if error catched add it in form
	try
	{
    $cat->edit($cat_requested->id, [
			'name' => ucfirst(f('name'))
		]);
		Utils::redirect(PLUGIN_URL . 'categories/index.php');
	}
  catch (\RuntimeException $e)
	{
		if (strstr($e->getMessage(), 'UNIQUE constraint failed'))
		{
			$form->addError('Cette catégorie existe déjà.');
		} else
		{
			$form->addError($e->getMessage());
		}
	}
}

$cancel_link = PLUGIN_URL . 'categories/index.php';

// send all to template
$tpl->assign(compact('cat_requested', 'csrf_key', 'cancel_link'));

$tpl->display(PLUGIN_ROOT . '/templates/categories/modifier_categorie.tpl');
