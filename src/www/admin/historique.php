<?php

// historic of movements (entry and output) for a specific Equipment

namespace Garradin;
use Garradin\Plugin\Materiels\Equipment;
use Garradin\Plugin\Materiels\Category;
use Garradin\Plugin\Materiels\Movement;

require_once __DIR__ . '/_inc.php';

$eqmt = new Equipment;
$cat = new Category;
$mvt = new Movement;

// get equipment requested, his category's name and all of his movements
$eqmt_requested = $eqmt->get((int) qg('id'));
$eqmt_cat = $cat->get($eqmt_requested->category_id);
$eqmt_requested->category = $eqmt_cat->name;
$mvts = $mvt->AllEqmtMovements($eqmt_requested->id);

$return_link = PLUGIN_URL . 'index.php';

// send all to template
$tpl->assign(compact('eqmt_requested', 'mvts', 'return_link'));

$tpl->display(PLUGIN_ROOT . '/templates/historique.tpl');
