<?php

// edit a specific equipment

namespace Garradin;

require_once __DIR__ . '/_inc.php';

use Garradin\Plugin\Materiels\Equipment;
use Garradin\Plugin\Materiels\Category;
use Garradin\Utils;

// get the equipment requested
$eqmt = new Equipment;
$eqmt_requested = $eqmt->get((int) qg('id'));

if (!$eqmt_requested) {
	throw new UserException("Ce matériel n'existe pas.");
}

// get all categories and set the selected one
$cat = new Category;
$cats = $cat->listAll();
$selected_cat = $eqmt_requested->category_id;

// check if edit form is submitted
$csrf_key = 'edit_equipment_' . $eqmt_requested->id;

if (f('save') && $form->check($csrf_key) && !$form->hasErrors())
{
	// try to edit equipment selected and if error catched add it in form
	try
	{
    $eqmt->edit($eqmt_requested->id, [
      'category_id' => (int) f('category_id'),
      'designation' => ucfirst(strtolower(f('designation'))),
		]);
		Utils::redirect(PLUGIN_URL . 'index.php');
	}
  catch (\RuntimeException $e)
	{
		if (strstr($e->getMessage(), 'UNIQUE constraint failed'))
		{
			$form->addError('Ce matériel avec cette désignation existe déjà.');
		} else
		{
			$form->addError($e->getMessage());
		}
	}
}

$cancel_link = PLUGIN_URL . 'index.php';

// send all to template
$tpl->assign(compact('eqmt_requested', 'cats', 'selected_cat', 'csrf_key', 'cancel_link'));

$tpl->display(PLUGIN_ROOT . '/templates/modifier_materiel.tpl');
