<?php

namespace Garradin;

$session->requireAccess($session::SECTION_USERS, $session::ACCESS_WRITE);

$tpl->assign('plugin_tpl', PLUGIN_ROOT . '/templates/');
