<?php

namespace Garradin;

require_once __DIR__ . '/../_inc.php';

// get the list of all entries and send it to template

$mvts = $mvt->listAllOneSide(0);

$tpl->assign(compact('mvts'));

$tpl->display(PLUGIN_ROOT . '/templates/mouvements/entrees/index.tpl');
