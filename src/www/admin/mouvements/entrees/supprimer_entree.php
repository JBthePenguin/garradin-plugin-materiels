<?php

// delete a specific entry

namespace Garradin;

require_once __DIR__ . '/../_inc.php';

use Garradin\Plugin\Materiels\Equipment;
use Garradin\Utils;

// get the entry to delete
$entry_to_delete = $mvt->get((int) qg('id'));
if (!$entry_to_delete)
{
	throw new UserException("Cette entrée n'existe pas.");
}

// check if delete form is submitted
$eqmt = new Equipment;

$csrf_key = 'delete_entry_' . $entry_to_delete->id;

if (f('delete') && $form->check($csrf_key) && !$form->hasErrors())
{
	// delete the movement required
	$mvt->delete($entry_to_delete->id);
	// try to delete the corresponding equipment
	// and pass if it's not possible (used in an another movement)
	try
	{
		$eqmt->delete($entry_to_delete->equipment_id);
	}
	catch (\RuntimeException $e){
	}
	Utils::redirect(PLUGIN_URL . 'mouvements/entrees/index.php');
}

$corresponding_eqmt = $eqmt->get($entry_to_delete->equipment_id);

// check if it's possible to delete this entry
if ($mvt->PossibilityDeleteEntry($entry_to_delete))
{
	// it's possible
  $cancel_link = PLUGIN_URL . 'mouvements/entrees/index.php';
	// construct string to send to template
  $entry_string = (string) $entry_to_delete->equipment_number . " " . $corresponding_eqmt->designation . " à la date du " . date_create_from_format(
    "Y-m-d", $entry_to_delete->mvt_date)->format("d/m/y");

  $eqmt_name = $corresponding_eqmt->designation;

  $tpl->assign(compact('entry_string', 'eqmt_name', 'csrf_key', 'cancel_link'));

  $tpl->display(PLUGIN_ROOT . '/templates/mouvements/entrees/supprimer_entree.tpl');

} else {
	// not possible, display error message
  throw new UserException(
		"Cette entrée ne peut pas être supprimée car ça rendrait impossible l'historique des entrées et sorties de « " . $corresponding_eqmt->designation . " ». --- plus de sorties que d'entrées ! ---");
}
