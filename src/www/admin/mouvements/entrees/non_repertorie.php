<?php

// add entry for materiel not listed

namespace Garradin;

use Garradin\Plugin\Materiels\Category;
use Garradin\Plugin\Materiels\Equipment;
use Garradin\Utils;

require_once __DIR__ . '/../_inc.php';

// get the list of entry's kinds
$kinds = $mvt->listEntryKinds();
$selected_kind = $kinds[0];

// get list of all categories and make the first one as default
$cat = new Category;
$cats = $cat->listAll();
$selected_cat = $cats[0]->id;

// check if add form is submitted
$csrf_key = 'add_entry';

if (f('save') && $form->check($csrf_key) && !$form->hasErrors())
{
  // try to add new equipment, get his id, add new entry
  // and if error catched add it in form
  try
  {
    // add new equipment and get his id
    $eqmt = new Equipment;
  	$eqmt_id = $eqmt->add([
  		'category_id' => (int) f('category_id'),
      'designation' => ucfirst(strtolower(f('designation'))),
  	]);
    // make the entry date in the good format
    $mvt_date_format = date_create_from_format(
      "d/m/Y", f('mvt_date'))->format("Y-m-d");
    // add new entry
    $mvt->add([
      'side' => 0,
      'kind' => f('kind'),
      'equipment_number' => (int) f('equipment_number'),
      'equipment_id' => $eqmt_id,
      'mvt_date' => $mvt_date_format,
      'additional_comment' => f('additional_comment'),
    ]);
  	Utils::redirect(PLUGIN_URL . 'mouvements/entrees/index.php');
  }
  catch (\RuntimeException $e)
  {
  	if (strstr($e->getMessage(), 'UNIQUE constraint failed'))
  	{
  		$form->addError('Un matériel avec cette désignation est déjà répertorié.');
  	} else
  	{
  		$form->addError($e->getMessage());
  	}
    // keep the datas submitted as selected
    $selected_kind = f('kind');
    $selected_cat = f('category_id');
  }
}

//  make default date (now)
$default_date = new \DateTime;

// make comment placeholder
$comment_placeholder = "ex: don reçu de la part de...";

// make cancel link, legend for the title of the form
// and the template name for equipment to use in form
$cancel_link = PLUGIN_URL . 'mouvements/entrees/index.php';
$legend_part = "non répertorié";
$tpl_materiel_name = "non_repertorie";

// send all to template

$tpl->assign(compact(
  'kinds', 'cats', 'selected_kind', 'default_date',
  'comment_placeholder', 'selected_cat',
  'cancel_link', 'legend_part', 'tpl_materiel_name', 'csrf_key'
));

$tpl->display(PLUGIN_ROOT . '/templates/mouvements/entrees/ajouter_entree.tpl');
