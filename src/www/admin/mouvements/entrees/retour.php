<?php

// add entry for materiel in return

namespace Garradin;

require_once __DIR__ . '/../_inc.php';

use Garradin\Plugin\Materiels\Equipment;
use Garradin\Utils;

// get the list of equipments for possible return (loan or rent)
$eqmt = new Equipment;
$eqmts_by_cat = $eqmt->listAllReleasedRentByCategory();
$selected_eqmt = "";

// check if add form is submitted
$csrf_key = 'add_entry';

if (f('save') && $form->check($csrf_key) && !$form->hasErrors())
{
  $eqmt_id = f('equipment_id');
  $eqmt_number = (int) f('equipment_number');
  $mvt_date_format = date_create_from_format(
    "d/m/Y", f('mvt_date'))->format("Y-m-d");
  // check if it's possible to add this return entry
  if ($mvt->PossibilityRentEqmtEntry($eqmt_id, $eqmt_number, $mvt_date_format))
  {
    // it's possible, add new entry
    $mvt->add([
      'side' => 0,
      'kind' => 'Retour de location / prêt',
      'equipment_number' => $eqmt_number,
      'equipment_id' => $eqmt_id,
      'mvt_date' => $mvt_date_format,
      'additional_comment' => f('additional_comment'),
    ]);
  	Utils::redirect(PLUGIN_URL . 'mouvements/entrees/index.php');
  } else
  {
    // not possible, add error to form
    $equiment = $eqmt->get($eqmt_id);
    $form->addError(
      "Il est impossible de rentrer " . (string) $eqmt_number . " " . $equiment->designation . " à la date du " . (string) f('mvt_date') . '.');
    // keep the datas submitted as selected
    $selected_eqmt = $eqmt_id;
  }
}

//  make default date (now)
$default_date = new \DateTime;

// make comment placeholder
$comment_placeholder = "ex: retour de prêt...";

// make cancel link, legend for the title of the form
// and the template name for equipment to use in form
$cancel_link = PLUGIN_URL . 'mouvements/entrees/index.php';
$legend_part = "en retour de location / prêt";
$tpl_materiel_name = "retour";

// no kinds needed
$kinds = false;
$selected_kind = false;

// send all to template
$tpl->assign(compact(
  'eqmts_by_cat', 'default_date', 'cancel_link', 'legend_part', 'comment_placeholder', 'tpl_materiel_name', 'csrf_key', 'selected_eqmt',
  'kinds', 'selected_kind',
));

$tpl->display(PLUGIN_ROOT . '/templates/mouvements/entrees/ajouter_entree.tpl');
