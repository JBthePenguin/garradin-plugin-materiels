<?php

// add entry for materiel listed

namespace Garradin;

require_once __DIR__ . '/../_inc.php';

use Garradin\Plugin\Materiels\Equipment;
use Garradin\Utils;

// check if add form is submitted
$csrf_key = 'add_entry';

if (f('save') && $form->check($csrf_key) && !$form->hasErrors())
{
  // make the entry  date in the good format
  $mvt_date_format = date_create_from_format(
    "d/m/Y", f('mvt_date'))->format("Y-m-d");
  // add new entry
  $mvt->add([
    'side' => 0,
    'kind' => f('kind'),
    'equipment_number' => (int) f('equipment_number'),
    'equipment_id' => f('equipment_id'),
    'mvt_date' => $mvt_date_format,
    'additional_comment' => f('additional_comment'),
  ]);
  Utils::redirect(PLUGIN_URL . 'mouvements/entrees/index.php');
}

// get the list of all equipments ordered by category
$eqmt = new Equipment;
$eqmts_by_cat = $eqmt->listAllByCategory();

// get the list of entry's kinds
$kinds = $mvt->listEntryKinds();
$selected_kind = $kinds[0];

// make default date (now)
$default_date = new \DateTime;

// make comment placeholder
$comment_placeholder = "ex: don reçu de la part de...";

// make cancel link, legend for the title of the form
// and the template name for equipment to use in form
$cancel_link = PLUGIN_URL . 'mouvements/entrees/index.php';
$legend_part = "répertorié";
$tpl_materiel_name = "repertorie";

// send all to template

$tpl->assign(compact(
  'kinds', 'eqmts_by_cat', 'selected_kind', 'default_date',
  'comment_placeholder', 'cancel_link',
  'legend_part', 'tpl_materiel_name', 'csrf_key'
));

$tpl->display(PLUGIN_ROOT . '/templates/mouvements/entrees/ajouter_entree.tpl');
