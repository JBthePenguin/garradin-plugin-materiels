<?php

// add output for a borrowed equipment

namespace Garradin;

require_once __DIR__ . '/../_inc.php';

use Garradin\Plugin\Materiels\Equipment;
use Garradin\Utils;

$eqmt = new Equipment;
$selected_eqmt = "";

// check if add form is submitted
$csrf_key = 'add_output';

if (f('save') && $form->check($csrf_key) && !$form->hasErrors())
{
  $eqmt_id = f('equipment_id');
  $eqmt_number = (int) f('equipment_number');
  $mvt_date_format = date_create_from_format(
    "d/m/Y", f('mvt_date'))->format("Y-m-d");
  // check if it's possible to output this equipment
  if ($mvt->PossibilityNoOwnedEqmtOutput($eqmt_id, $eqmt_number, $mvt_date_format))
  {
    // it's possible, add new output
    $mvt->add([
      'side' => 1,
      'kind' => 'Retour de location / prêt',
      'equipment_number' => $eqmt_number,
      'equipment_id' => $eqmt_id,
      'mvt_date' => $mvt_date_format,
      'additional_comment' => f('additional_comment'),
    ]);
    Utils::redirect(PLUGIN_URL . 'mouvements/sorties/index.php');
  } else
  {
    // not possible, add error to form
    $equiment = $eqmt->get($eqmt_id);
    $form->addError(
      "Il est impossible de sortir " . (string) $eqmt_number . " " . $equiment->designation . " à la date du " . (string) f('mvt_date') . '.');
    // keep the datas submitted as selected
    $selected_eqmt = $eqmt_id;
  }
}

// get list of borrowed equipments ordered by category
$eqmts_by_cat = $eqmt->ListAllBorrowedByCategory();

//  make default date (now)
$default_date = new \DateTime;

// make comment placeholder
$comment_placeholder = "ex: matériel rendu...";

// make cancel link, legend for the title of the form
// and the template name for equipment to use in form
$cancel_link = PLUGIN_URL . 'mouvements/sorties/index.php';
$legend_part = "emprunté";
$tpl_materiel_name = "emprunte";

// no kinds needed
$kinds = false;
$selected_kind = false;

// send all to template
$tpl->assign(compact(
  'csrf_key', 'cancel_link', 'legend_part', 'tpl_materiel_name',
  'selected_eqmt', 'default_date', 'comment_placeholder',
  'eqmts_by_cat', 'kinds', 'selected_kind'));

$tpl->display(PLUGIN_ROOT . '/templates/mouvements/sorties/ajouter_sortie.tpl');
