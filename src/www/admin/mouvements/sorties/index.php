<?php

namespace Garradin;

require_once __DIR__ . '/../_inc.php';

// get the list of all outputs and send it to template

$mvts = $mvt->listAllOneSide(1);

$tpl->assign(compact('mvts'));

$tpl->display(PLUGIN_ROOT . '/templates/mouvements/sorties/index.tpl');
