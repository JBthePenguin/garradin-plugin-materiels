<?php

// delete a specific entry

namespace Garradin;

require_once __DIR__ . '/../_inc.php';

use Garradin\Plugin\Materiels\Equipment;
use Garradin\Utils;

// get the output to delete
$output_to_delete = $mvt->get((int) qg('id'));
if (!$output_to_delete)
{
	throw new UserException("Cette sortie n'existe pas.");
}

// check if delete form is submitted
$csrf_key = 'delete_output_' . $output_to_delete->id;

if (f('delete') && $form->check($csrf_key) && !$form->hasErrors())
{
	// delete the movement required
	$mvt->delete($output_to_delete->id);
	Utils::redirect(PLUGIN_URL . 'mouvements/sorties/index.php');
}

$eqmt = new Equipment;
$corresponding_eqmt = $eqmt->get($output_to_delete->equipment_id);

// check if it's possible to delete this output
if ($mvt->PossibilityDeleteOutput($output_to_delete))
{
	// it's possible
  $cancel_link = PLUGIN_URL . 'mouvements/sorties/index.php';
	// construct string to send to template
  $output_string = (string) $output_to_delete->equipment_number . " " . $corresponding_eqmt->designation . " à la date du " . date_create_from_format(
    "Y-m-d", $output_to_delete->mvt_date)->format("d/m/y");

  $tpl->assign(compact('output_string', 'csrf_key', 'cancel_link'));

  $tpl->display(PLUGIN_ROOT . '/templates/mouvements/sorties/supprimer_sortie.tpl');

} else {
  throw new UserException(
		"Cette sortie ne peut pas être supprimée car ça rendrait impossible l'historique des entrées et sorties de « " . $corresponding_eqmt->designation . " ». --- plus de 'retour de location / prêt' en entrée que de 'location / prêt' en sortie ! ---");
}
