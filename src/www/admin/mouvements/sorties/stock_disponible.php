<?php

// add output for an available equipment

namespace Garradin;

require_once __DIR__ . '/../_inc.php';

use Garradin\Plugin\Materiels\Equipment;
use Garradin\Utils;

// get the list of all available equipments ordered by category
$eqmt = new Equipment;

$eqmts_by_cat = $eqmt->ListAllAvailableByCategory();
$selected_eqmt = "";

// get the list of output's kinds
$kinds = $mvt->listOutputKinds();
$selected_kind = $kinds[0];

// check if add form is submitted
$csrf_key = 'add_output';

if (f('save') && $form->check($csrf_key) && !$form->hasErrors())
{
  $eqmt_id = f('equipment_id');
  $eqmt_number = (int) f('equipment_number');
  $mvt_date_format = date_create_from_format(
    "d/m/Y", f('mvt_date'))->format("Y-m-d");
  // check if it's possible to output this equipment
  if ($mvt->PossibilityOwnedEqmtOutput($eqmt_id, $eqmt_number, $mvt_date_format))
  {
    // it's possible, add new output
    $mvt->add([
      'side' => 1,
      'kind' => f('kind'),
      'equipment_number' => $eqmt_number,
      'equipment_id' => $eqmt_id,
      'mvt_date' => $mvt_date_format,
      'additional_comment' => f('additional_comment'),
    ]);
    Utils::redirect(PLUGIN_URL . 'mouvements/sorties/index.php');
  } else
  {
    // not possible, add error to form
    $equiment = $eqmt->get($eqmt_id);
    $form->addError(
      "Il est impossible de sortir " . (string) $eqmt_number . " " . $equiment->designation . " à la date du " . (string) f('mvt_date') . '.');
    // keep the datas submitted as selected
    $selected_eqmt = $eqmt_id;
    $selected_kind = f('kind');
  }
}

//  make default date (now)
$default_date = new \DateTime;

// make comment placeholder
$comment_placeholder = "ex: don fait à...";

// make cancel link, legend for the title of the form
// and the template name for equipment to use in form
$cancel_link = PLUGIN_URL . 'mouvements/sorties/index.php';
$legend_part = "en stock disponible";
$tpl_materiel_name = "stock_disponible";

$tpl->assign(compact(
  'csrf_key', 'cancel_link', 'legend_part', 'tpl_materiel_name',
  'kinds', 'selected_eqmt', 'selected_kind', 'default_date',
  'comment_placeholder', 'eqmts_by_cat'));

$tpl->display(PLUGIN_ROOT . '/templates/mouvements/sorties/ajouter_sortie.tpl');
