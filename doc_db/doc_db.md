## Plugin materiels database

![img](./schema.png)

### Table plugin_materiels_category
| Idx | Field Name | Data Type |
|---|---|---|
| *🔑 ⬋ | id| integer autoincrement |
| *🔍 | name| varchar(100)  |
| Indexes |
| 🔑 primary key | pk_plugin_materiels_category ||
| 🔍 unique  | u_category_name |

### Table plugin_materiels_equipment
| Idx | Field Name | Data Type |
|---|---|---|
| *🔑 ⬋ | id| integer autoincrement |
| *⬈ | category_id| integer  |
| *🔍 | designation| varchar(255)  |
| Indexes |
| 🔑 primary key | pk_plugin_materiels_equipment || ON id|
| 🔍 unique | u_equipment_designation || ON designation|
| Foreign Keys |
|  | Fk_plugin_materiels_equipment | ( category_id ) ref plugin_materiels_category (id) |

### Table plugin_materiels_movement
| Idx | Field Name | Data Type | Description |
|---|---|---|---|
| *🔑 | id| integer autoincrement |  |
| *| side| boolean  | 0 -> entry, 1 -> output |
| *| kind| varchar(20)  | Achat, Don, Récupération, ... |
| *| equipment_number| integer  |  |
| *⬈ | equipment_id| integer  |  |
| *| mvt_date| date  |  |
|  | additional_comment| varchar(255)  |  |
| Indexes |
| 🔑 primary key | pk_plugin_materiels_movement |  |
| Foreign Keys |
|  | Fk_plugin_materiels_movement | ( equipment_id ) ref plugin_materiels_equipment (id) |  |

| * -> NOT NULL
